(function() {
	$(document).ready(function(){
		$(".skills-slides").owlCarousel({
			items: 5,
			nav: true,
			margin: 20,
			responsive : {
				0: {
					items: 1,
				},
				550: {
					items: 2,
				},
				730: {
					items: 3,
				},
				950: {
					items: 4,
				},
				1140: {
					items: 5,
				}
			}
		});
		$(".format-certificates-slides").owlCarousel({
			items: 1,
			nav: true
		});
		$(".mass-media-slides").owlCarousel({
			items: 6,
			nav: true,
			responsive : {
				0: {
					items: 2,
				},
				550: {
					items: 3,
				},
				730: {
					items: 4,
				},
				950: {
					items: 5,
				},
				1140: {
					items: 6,
				}
			}
		});
		$(".reviews-video-slides").owlCarousel({
			items: 1,
			nav: true,
			video: true,
			onPlayVideo: function() {
				$('.owl-item.active .reviews-video-item-inside').addClass('hidden');
			}
		});

		$('.program-free').click(function(e){
			e.preventDefault();
			$('.mask, .modal').removeClass('hidden');
			$('body').addClass('scroll-disabled');
		});

		$('.mask').click(function(e){
			var iframe = $('body > iframe:not(.hidden)');

			if (iframe.length) {
				var index = iframe.attr('src').indexOf('&autoplay=1');
				$(iframe).attr('src', $(iframe).attr('src').slice(0, index));
			}

			$('.mask, .modal, body > iframe').addClass('hidden');
			$('body').removeClass('scroll-disabled');
		});

		$('.course-module button').click(function(e){
			$(this).toggleClass('rotate');
			$(this).siblings('.course-module-desc').toggleClass('expanded');
		});

		// modal video play
		$('.modal-play').click(function(e){
			var iframe = $('.modal-video[data-video=' + $(this).attr('data-play') + ']');
			iframe.removeClass('hidden').attr('src', iframe.attr('src') + '&autoplay=1');
			$('.mask').removeClass('hidden');
			$('body').addClass('scroll-disabled');
		});

		$('.program-price, .course-module-reg, .format-registration').click(function(e){
			e.preventDefault();
			var button = $(this).data('reg');
			$('html, body').animate({scrollTop:$('*[data-block='+button+']').offset().top}, 1000);
		});

		$('.flexslider').flexslider({
			controlNav: "thumbnails",
			slideshow: false,
			animationLoop: false,
			prevText: '',
			nextText: '',
		});

		$('.team-video-wrapper-play').click(function() {
			var iframeCurrent = $(this).parent().siblings('iframe');
			$(this).parent().addClass('hidden');
			iframeCurrent.attr('src', iframeCurrent.attr('src') + '?autoplay=1');
		});

		$('.reviews-video-item-inside-play').click(function() {
			var iframesAll = $('.reviews-video iframe'),
				iframeCurrent = $(this).parent().siblings('iframe');
			iframesAll.each(function(i, iframe) {
				var index = $(iframe).attr('src').indexOf('?autoplay=1');
				if (index != -1) {
					$(iframe).attr('src', $(iframe).attr('src').slice(0, index + 1));
				}
			})
			$(this).parent().addClass('hidden');
			iframeCurrent.attr('src', iframeCurrent.attr('src') + '?autoplay=1');
		});

		$('.reviews-video .flex-control-thumbs li').click(function() {
			setTimeout(function() {
				$('.flex-active-slide .reviews-video-item-inside-play').trigger('click');
			}, 500)
		});

		$('.flex-direction-nav').click(function() {
			var top = $('.flex-control-thumbs li img.flex-active').parent('li')[0].offsetTop;
			$('.flex-control-thumbs').animate({scrollTop: top}, 500);
		});

		$('.flex-control-thumbs').addClass('scrollbar-inner').scrollbar();
	});
})();