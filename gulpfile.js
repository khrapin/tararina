'use strict'

let gulp = require('gulp'),
	scss = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	minify = require('gulp-clean-css'),
	autoprefixer = require('gulp-autoprefixer');

gulp.task('default', function() {
	return gulp.watch('src/*.scss', ['scss']);
});

gulp.task('scss', function() {
	return gulp.src('./src/*.scss')
		.pipe(sourcemaps.init())
		.pipe(autoprefixer({browsers: ['last 2 versions']}))
		.pipe(scss())
  		.pipe(minify())
  		.pipe(sourcemaps.write())
  		.pipe(gulp.dest('build/css'));
});

// comment 1
